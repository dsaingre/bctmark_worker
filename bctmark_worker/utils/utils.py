import yaml


def read_transactions(transaction_file):
    with open(transaction_file, 'r') as f:
        transactions = yaml.safe_load(f)
    return transactions
