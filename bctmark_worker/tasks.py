from bctmark_worker.utils import read_transactions
from bctmark_worker.clients import Eth


def replay(transactions_file, port='8545'):
    transactions = read_transactions(transactions_file)
    e = Eth('localhost', port)
    e.replay_all(transactions)
