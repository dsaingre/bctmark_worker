import click
import logging
import os
import bctmark_worker.tasks as t

logging.basicConfig(level=logging.INFO)


@click.group()
def cli():
    pass


@cli.command(help="replay transactions")
@click.argument("transactions_file")
@click.option("--port",
              default='8545',
              help="port of blockchain peers")
def replay(transactions_file, port):
    t.replay(os.path.abspath(transactions_file), port)
