from copy import copy
from typing import Dict

from web3 import Web3, HTTPProvider
from web3.middleware import geth_poa_middleware

import logging
import time


class Eth:
    def __init__(self, target_url='localhost', target_port='8545'):
        logging.basicConfig(filename='/tmp/bctmark-worker.log',
                            filemode='a',
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.DEBUG)
        self.logger = logging.getLogger()
        self.target_url = target_url
        self.target_port = target_port
        self.w3 = Web3(HTTPProvider('http://%s:%s' % (self.target_url, self.target_port)))
        self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        logging.debug("BCTMARK INIT")

    def replay_all(self, transactions):
        transactions_to_replay = copy(transactions)
        transactions_contract_creation = [t for t in transactions_to_replay if t['to'] is None]
        nb_transactions_contract_creation = len(transactions_contract_creation)
        print(nb_transactions_contract_creation)
        self.logger.debug("Retrieved %s contract creation to replay" % nb_transactions_contract_creation)
        transactions_without_contract_creation = [t for t in transactions_to_replay if t['to'] is not None]
        nb_transactions_without_contract_creation = len(transactions_without_contract_creation)
        print(nb_transactions_without_contract_creation)
        self.logger.debug("Retrieved %s transactions to replay" % nb_transactions_without_contract_creation)

        start_time = time.time()
        for i in range(nb_transactions_contract_creation):
            t = transactions_contract_creation[i]
            self.logger.debug("Creating contract %s of %s" % (i, nb_transactions_contract_creation))
            trxhex = self._send_transaction(t)
            receipt = self.w3.eth.waitForTransactionReceipt(trxhex)
            new_contract_address = receipt['contractAddress']
            for t_without_contract_creation in transactions_without_contract_creation:
                if t_without_contract_creation['to'] == t['contractAddress']:
                    t_without_contract_creation.update({'to': new_contract_address})
        self.logger.debug("Contract creation done in %s seconds" % (time.time() - start_time))

        start_time = time.time()
        for i in range(nb_transactions_without_contract_creation):
            t = transactions_without_contract_creation[i]
            self.logger.debug("Sending transaction %s of %s" % (i, nb_transactions_without_contract_creation))
            self._send_transaction(t)
        self.logger.debug("Transaction replay done in %s seconds" % (time.time() - start_time))

    def _send_transaction(self, transaction) -> Dict:
        hash = self.w3.eth.sendTransaction({
            "from": Web3.toChecksumAddress(transaction['from']),
            "to": Web3.toChecksumAddress(transaction['to']) if transaction['to'] is not None else None,
            "value": transaction['value'],
            "data": transaction['input']
        })
        return hash.hex()
